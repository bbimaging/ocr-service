import boto3, json, os, pytesseract, cv2
from skimage import io
from dotenv import load_dotenv

load_dotenv()

print("ocr-service")

AWS_REGION=os.environ['AWS_REGION']
AWS_KEY=os.environ['AWS_KEY']
AWS_SECRET=os.environ['AWS_SECRET']
URL_PREFIX=os.environ['URL_PREFIX']
FRAME_READY_URL=os.environ['FRAME_READY_URL']
FRAME_UPDATED_URL=os.environ['FRAME_UPDATED_URL']

sqs_client = boto3.client("sqs", 
    region_name=AWS_REGION,
    aws_access_key_id=AWS_KEY, 
    aws_secret_access_key=AWS_SECRET, 
)

def eval_image(_image):
    download = io.imread(_image)
    image = cv2.cvtColor(download, cv2.COLOR_RGB2BGR)

    x,y,w,h = 100, 525, 550, 593  
    ROI = 255 - image[y:y+h,x:x+w]
    data = pytesseract.image_to_string(ROI).replace('\n','').strip()

    # cv2.imshow('ROI', ROI)
    # cv2.waitKey()
    return data

def delete_message(queue_url, receipt_handle):
    sqs_client = boto3.client("sqs", region_name="us-west-2")
    response = sqs_client.delete_message(
        QueueUrl=queue_url,
        ReceiptHandle=receipt_handle,
    )
    print(response)

def send_message(data):
    return sqs_client.send_message(
        QueueUrl=FRAME_UPDATED_URL,
        MessageBody=data
    )

def get_messages():
    response = sqs_client.receive_message(
        QueueUrl=FRAME_READY_URL,
        MaxNumberOfMessages=1,
        WaitTimeSeconds=5,
    )

    for message in response.get("Messages", []):
        receipt_handle = message['ReceiptHandle']
        message_body = message["Body"]
        body = json.loads(message_body)
        _id = body['_id']
        key = body['key']
        url = URL_PREFIX + key
        data = eval_image(url)

        print(f"key: {key}")
        print(f"url: {url}")
        print(f"data: {data}")

        send_message(json.dumps({ "_id": _id, "description": data }))

        delete_message(FRAME_READY_URL, receipt_handle)
    return None

while True:
    get_messages()