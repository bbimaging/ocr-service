FROM jjanzic/docker-python3-opencv

ADD ocr.py /opt/build/

RUN python3 -m pip install -U scikit-image
RUN pip3 install boto3
RUN pip3 install pytesseract
RUN pip3 install python-dotenv

CMD [ "python", "ocr.py" ]