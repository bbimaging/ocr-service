# bbimaging #
## ocr-service ##

Environment variables:
```
AWS_REGION=us-west-2
AWS_KEY=
AWS_SECRET=
URL_PREFIX=https://bbimaging-0.s3.us-west-2.amazonaws.com/
FRAME_READY_URL=https://sqs.us-west-2.amazonaws.com/791342033319/FRAME_READY
FRAME_UPDATED_URL=https://sqs.us-west-2.amazonaws.com/791342033319/FRAME_UPDATED
```

Docker commands:

Build:
```
$ docker build -t davidjbarnes/bbimaging-ocr-service .
```

Run:
```
$ docker run -it --rm --env-file .env davidjbarnes/bbimaging-ocr-service
```